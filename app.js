let counter = 0;
const matrixArr =[
  [
  [0, 1, 0, 1, 0],
  [0, 1, 1, 1, 0],
  [0, 1, 1, 1, 0],
  [0, 1, 1, 0, 1],
  [1, 1, 1, 1, 1]
  ],
  [
  [1, 0, 0, 1, 0],
  [1, 1, 1, 0, 0],
  [1, 1, 1, 1, 0],
  [0, 1, 1, 0, 1],
  [1, 1, 1, 0, 0]
  ],
  [
  [1, 1, 1, 0, 0],
  [0, 0, 1, 0, 0],
  [1, 1, 1, 1, 0],
  [0, 1, 1, 0, 1],
  [1, 0, 1, 0, 0]
  ],
];

const marker = (element) => {
  element.hasClass("blank") ? null : element.toggleClass("on")
}

const winChecker = () => {
  counter++;
  $(".score").text(`Liczba klikniec ${counter}`)
  if ($("td:not(.on):not(.blank)").length === 0){
    $('tr > td').addClass('endgame');
    $(".title").text("Ukladanka: Zwyciestwo");
  }
}

const generateRandomMesh = () => {
  let matrix = matrixArr[Math.floor(Math.random() * 3)];
  for (i = 0; i < matrix.length; ++i) {
    items = [];
    for (j = 0; j < matrix[0].length; ++j) {
        if (matrix[i][j] === 0){
          items.push(`<td class="blank"> </td>`)
        }else if (matrix[i][j] === 1) {
          items.push(`<td> </td>`)
        }
    }
    $("#game").append(`<tr> ${items} </tr>`)

  }
}

$(document).ready(function(){
    generateRandomMesh();

    $("td:not(.blank):not(endgame)").click(function(e){
      if($(this).hasClass("endgame"))
        return null;
        
      index = e.target.cellIndex

      marker($(this))
      marker($(this).next())
      marker($(this).prev())
      marker($(this).parent().prev().children().eq(index))
      marker($(this).parent().next().children().eq(index))

      winChecker()
    });

});
